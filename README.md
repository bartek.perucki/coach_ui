# Coach UI

## Starting

To start the UI make sure that the API is running on port `3001`, then run:

Development:
```
yarn run dev-server
```

## ToDo:

- ? use Redux store to keep the JWT (so far in `localStorage`)
- x style for new trainings
- x Don't show trainings before athlete was created
- x Dictionary
- moment.locale(LANGUAGE) for the Calendar
- Handle user language settings
- Merge Trainings and Results -> Calendar
- Handle athlete.createdAt vs. firstTraining
- replace training selectors by attendance selectors
- move redux actions calls to queries
- add Jest tests
- Restyle the app
- login / signin forms
- change group.index to group.id in training store