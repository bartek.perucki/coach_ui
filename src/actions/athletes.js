import {
  SET_ATHLETES,
  ADD_ATHLETE,
  UPDATE_ATHLETE,
  DELETE_ATHLETE,
} from "../reducers/athletes";

export const setAthletes = athletes => ({
  type: SET_ATHLETES,
  athletes
});

export const addAthlete = athlete => ({
  type: ADD_ATHLETE,
  athlete
});

export const updateAthlete = (id, updates) => ({
  type: UPDATE_ATHLETE,
  id: id,
  updates: updates
});

export const deleteAthlete = id => ({
  type: DELETE_ATHLETE,
  id
});
