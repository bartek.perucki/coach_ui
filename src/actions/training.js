import {
  SET_DATE,
  SET_CATEGORY,
  INCREMENT_STEP,
  DECREMENT_STEP,
  ADD_TRAINING_GROUP,
  RESET_TRAINING_GROUPS,
  SET_CURRENT_GROUP,
  RESET_STEP,
  RESET_TRAINING,
  ADD_EXCERCISE,
  REMOVE_EXCERCISE,
  SET_EXCERCISE_DESCRIPTION,
  SET_EXCERCISE_DESCRIPTIONS
} from '../reducers/training';

export const incrementStep = () => ({
  type: INCREMENT_STEP
});

export const decrementStep = () => ({
  type: DECREMENT_STEP
});

export const resetStep = () => ({
  type: RESET_STEP
})

export const resetTraining = () => ({
  type: RESET_TRAINING
})

export const setDate = (date) => ({
  type: SET_DATE,
  date,
});

export const setCategory = (category) => ({
  type: SET_CATEGORY,
  category,
});

export const addTrainingGroup = group => ({
  type: ADD_TRAINING_GROUP,
  group
});

export const resetTrainingGroups = () => ({
  type: RESET_TRAINING_GROUPS
})

export const setCurrentGroup = currentGroup => ({
  type: SET_CURRENT_GROUP,
  currentGroup
})

export const addExcerciseToGroup = (index, excercise) => ({
  type: ADD_EXCERCISE,
  index,
  excercise
})

export const removeExcerciseFromGroup = (index, excerciseId) => ({
  type: REMOVE_EXCERCISE,
  index,
  excerciseId
})

export const setGroupExcercisesDescription = (index, description) => ({
  type: SET_EXCERCISE_DESCRIPTION,
  index,
  description
})

export const setExcercisesDescription = descriptions => ({
  type: SET_EXCERCISE_DESCRIPTIONS,
  descriptions
})