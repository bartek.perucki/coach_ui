import {
  SET_TRAININGS,
  SET_LOADING_STATE
} from '../../reducers/training/trainingList';

export const setTrainings = trainings => ({
  type: SET_TRAININGS,
  trainings,
});

export const setLoadingState = state => ({
  type: SET_LOADING_STATE,
  state
})