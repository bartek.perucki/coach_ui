import { SET_ATTENDANCE } from '../reducers/attendance';

export const setAttendance = attendance => ({
  type: SET_ATTENDANCE,
  attendance
})
