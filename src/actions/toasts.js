import { toast } from 'react-toastify';

export const toastErrors = (errors) => {
  errors.map((e, index) => (
    toast.error(e, {
      position: toast.POSITION.TOP_RIGHT
    })
  ))
};

export const toastSuccess = (message) => {
  toast.success(message, {
    position: toast.POSITION.TOP_RIGHT
  })
}