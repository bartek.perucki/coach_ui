import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import pl from 'react-intl/locale-data/pl';
import localeData from '../public/locales/data.json';

import AppRouter, { history } from './routers/AppRouter.js';
import LoadingPage from './components/LoadingPage';
import configureStore from './store/configureStore';
import { login, logout } from './actions/auth';
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import 'react-dates/lib/css/_datepicker.css';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars } from '@fortawesome/free-solid-svg-icons'

library.add(faBars);
addLocaleData([...en, ...pl]);

const language = (navigator.languages && navigator.languages[0]) ||
                  navigator.language ||
                  navigator.userLanguage;
const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];
const messages = localeData[languageWithoutRegionCode] || localeData[language] || localeData.en;

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <IntlProvider locale={language} messages={messages}>
      <AppRouter />
    </IntlProvider>
  </Provider>
);

let hasRendered = false;
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('app'));
    hasRendered = true;
  }
};

ReactDOM.render(<LoadingPage />, document.getElementById('app'));

const token = localStorage.getItem('jwt');

// Find a way to rerender app.js

if (token) {
  store.dispatch(login(token));
  renderApp();
  if (history.location.pathname === '/login') {
    history.push('/dashboard');
  }
} else {
  store.dispatch(logout());
  renderApp();
  if (history.location.pathname !== '/signup') {
    history.push('/login');
  }
};

