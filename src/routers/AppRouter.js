import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';
import createHistory from 'history/createBrowserHistory';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/Authentication/LoginPage';
import SignupPage from '../components/Authentication/SignupPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import HomeIndex from '../components/Home/index';
import TrainingsIndex from '../components/Trainings/index';
import Results from '../components/Results/Results';

export const history = createHistory();

const DefaultRedirect = () => <Redirect to="/athletes"/>

const AppRouter = () => (
  <Router history={history}>
    <div>
      <ToastContainer transition={Slide} hideProgressBar={true} />
      <Switch>
        <PublicRoute path="/login" component={LoginPage} />
        <PublicRoute path="/signup" component={SignupPage} />
        
        <PrivateRoute path="/athletes" component={HomeIndex} />
        <PrivateRoute path="/trainings" component={TrainingsIndex} />

        <PrivateRoute path="/results" component={Results} exact={true} />
        <PrivateRoute path="/" component={DefaultRedirect} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;