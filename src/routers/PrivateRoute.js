import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import Header from '../components/Header/Header';
import Panel from '../components/Panel/Panel';

export const PrivateRoute = ({ 
  isAuthenticated, 
  component: Component, 
  ...rest
}) => (
  <Route {...rest} component={(props) => (
    isAuthenticated ? (
      <div className="app-screen">
        {/* <Panel /> */}
        <Header />
        <Component {...props} />
      </div>
    ) : (
      <Redirect to="/login" />
    )
  )} />
);

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.token
});

export default connect(mapStateToProps)(PrivateRoute);
