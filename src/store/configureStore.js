import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import authReducer from '../reducers/auth';
import createTrainingReducer from '../reducers/training/createTraining';
import trainingListReducer from '../reducers/training/trainingList';
import athletesReducer from '../reducers/athletes';
import attendanceReducer from '../reducers/attendance';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// Store creation

export default () => {
  const store = createStore(
    combineReducers({
      auth: authReducer,
      athletes: athletesReducer,
      attendance: attendanceReducer,
      training: combineReducers({
        create: createTrainingReducer,
        list: trainingListReducer,
      })
    }),

    composeEnhancers(applyMiddleware(thunk))
  );
  return store;
};
