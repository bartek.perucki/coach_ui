import React from 'react';
import { Link } from 'react-router-dom';

class DashboardHeader extends React.Component { 
  checkActive = (option) => {
    return this.props.active === option ? 'dashboard__link-active' : '';
  }

  render() {
    return(
      <div className="dashboard__header">
        <div className="dashboard__option">
          <Link
            to="/athletes"
            className={`dashboard__link ${this.checkActive('athletes')}`} 
            onClick={this.updateAthletes}
          >Athletes</Link>
        </div>
        <div className="dashboard__option">
          <Link
            to="/trainings"
            className={`dashboard__link ${this.checkActive('trainings')}`} 
            onClick={this.updateTrainings}
          >Trainings</Link>
        </div>
        <div className="dashboard__option">
          <Link
            to="/results"
            className={`dashboard__link ${this.checkActive('results')}`} 
            onClick={this.updateResults}
          >Results</Link>
        </div>
      </div>
    )
  }
};

export default DashboardHeader;