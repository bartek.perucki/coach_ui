import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import NotFoundPage from '../NotFoundPage';
import Attendace from '../Attendance/Attendance';
import Athlete from '../Athletes/Athlete';
import AddAthlete from '../Athletes/AddAthlete';

import getTrainingsQuery from '../Trainings/requests/getTrainings';
import getAthletesAttendanceQuery from '../Attendance/requests/getAthletesAttendance';
import getAthletesQuery from '../Athletes/requests/getAthletes';

import { setTrainings, setLoadingState } from '../../actions/training/trainingList';
import { setAttendance } from '../../actions/attendance';
import { setAthletes } from '../../actions/athletes';


class HomeIndex extends React.Component {

  componentDidMount() {
    const { setLoadingState, setTrainings, setAttendance, setAthletes } = this.props;

    setLoadingState(true);

    getTrainingsQuery().then((trainings) => {
      setTrainings(trainings);
      setLoadingState(false);
    });

    // getAthletesAttendanceQuery().then(attendance => {
      // setAttendance(attendance);
    // });

    getAthletesQuery().then((athletes) => {
      setAthletes(athletes);
    });
  }
  
  render(){
    return(
      <Switch>
        <Route path="/athletes/new" component={AddAthlete} />
        <Route path="/athletes/:id" component={Athlete} exact={true} />
        <Route path="/athletes" component={Attendace} exact={true} />
        <Route component={NotFoundPage} />
      </Switch>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    setTrainings: trainings => dispatch(setTrainings(trainings)),
    setLoadingState: isLoading => dispatch(setLoadingState(isLoading)),
    setAttendance: attendance => dispatch(setAttendance(attendance)),
    setAthletes: athletes => dispatch(setAthletes(athletes)),
  }
}

export default connect(undefined, mapDispatchToProps)(HomeIndex);