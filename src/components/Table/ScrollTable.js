import React from 'react';
import ScrollArea from 'react-scrollbar';
import Table from './Table';

class ScrollTable extends React.Component {
  fillHeaders = () => {
    let headers = [];

    if(this.props.headers) {
      this.props.headers.map((header, index) => {
        headers.push(
          <th key={index} className={`${this.props.contentClassPrefix}__header-${index}`} >
            { header }
          </th>
        );
      });
    }

    return(
      <thead>
        <tr>
          {headers}
        </tr>
      </thead>
    );
  }

  render() {
    return (
      <div className="table__scrollable">
        <table className="table">
          { this.fillHeaders() }
        </table>
        <ScrollArea
          speed={0.8}
          className="table__scrollable-area"
          horizontal={false}
        >
          <Table 
            noResults={this.props.noResults}
            noHeader={true}
            contentClassPrefix={"athletes"}
          >
            { this.props.children }
          </Table>
        </ScrollArea>
      </div>
    )
  }
}

export default ScrollTable;