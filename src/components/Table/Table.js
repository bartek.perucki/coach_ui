import React from 'react';

class Table extends React.Component {
  state = {
    columns: 0
  }

  componentWillMount() {
    let columnsNum = 0;

    if(this.props.headers && !this.props.noHeader) {
      columnsNum = this.props.headers.length;
    } else {
      columnsNum = this.props.columns;
    }

    this.setState(() => ({ columns: columnsNum }));
  }

  fillHeaders = () => {
    let headers = [];

    if(this.props.headers && !this.props.noHeader) {
      this.props.headers.map((header, index) => {
        headers.push(
          <th key={index} className={`${this.props.contentClassPrefix}__header-${index}`} >
            { header }
          </th>
        );
      });
    }

    return(
      <thead>
        <tr>
          {headers}
        </tr>
      </thead>
    );
  }

  render() {
    return(
      <table className="table">
        {this.fillHeaders()}
        <tbody>
          {
            this.props.children.length > 0 
            ?
              this.props.children
            :
              <tr>
                <td align="center" colSpan={this.state.columnsNum}>
                  {this.props.noResults}
                </td>
              </tr>
          }
        </tbody>
      </table>
    )
  };
};

export default Table;