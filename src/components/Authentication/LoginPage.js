import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import axios from 'axios';
import { toastErrors } from '../../actions/toasts';
import 'react-toastify/dist/ReactToastify.css';

import { login } from '../../actions/auth';
import { Link } from 'react-router-dom';


export class LoginPage extends React.Component {
  state = {
    errors: []
  }

  showErrors = () => {
    if (this.state.errors.length > 0) { 
      toastErrors(this.state.errors);
    }
  }

  handleLogin = (e) => {
    e.preventDefault();

    const email = e.target.email.value;
    const password = e.target.password.value;
    const request = {"auth": {"email": email, "password": password}};

    axios.post('http://localhost:3001/api/v1/user_token', request)
      .then((res) => {
        const token = res.data.jwt;
        localStorage.setItem("jwt", token);
        this.props.login(token);
      })
      .then(() => {
        this.props.history.push('/dashboard');
      })
      .catch(() => {
        this.setState(() => ({
          errors: ["Invalid username or password"]
        }))
      });
  }

  componentDidUpdate() {
    this.showErrors()
  }

  render() {
    return (
      <div className="box-layout">
        <div className="box-layout__box" >
          <h1 className="box-layout__title">
            <FormattedMessage
              id="Login.Header"
              defaultMessage="LOGIN"
            />
          </h1>

          <form onSubmit={this.handleLogin}>
            <label htmlFor="email">
              <FormattedMessage
                id="Login.Email"
                defaultMessage="Email:"
              />
            </label>
            <input
              name="email"
              id="email"
              type="email"
            />
            <label htmlFor="password">
              <FormattedMessage
                id="Login.Password"
                defaultMessage="Password"
              />
            </label>
            <input
              name="password"
              id="password"
              type="password"
            />
            <br />
            <FormattedMessage
              id="Login.Submit"
              defaultMessage="Submit"
            >
              {
                value => (
                  <input
                    value={value}
                    name="submit"
                    id="submit"
                    type="submit"
                    className="button"
                  />
                )
              }
            </FormattedMessage>
          </form>
          <p>
            <FormattedMessage
              id="Login.NewUser"
              defaultMessage="New User? "
            />
            <Link to="/signup">
              <FormattedMessage
                id="Login.SignUp"
                defaultMessage="Sign Up!"
              />
            </Link>!
          </p>
        </div>
      </div>
    )
  };
};

const mapDispatchToProps = (dispatch) => ({
  login: (token) => dispatch(login(token))
});

export default connect(undefined, mapDispatchToProps)(LoginPage);