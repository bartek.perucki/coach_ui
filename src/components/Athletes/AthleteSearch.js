import React from 'react';
import { DelayInput } from 'react-delay-input';

class AthleteSearch extends React.Component {
  handleChange = (e) => {
    this.props.updateFilter(e.target.value);
  }

  putCaretAtEnd = e => {
    // ToDo: Refactor
    const temp_value = e.target.value;
    e.target.value = '';
    e.target.value = temp_value;
  }

  render() {
    return(
      <form className="athletes__search-form">
        <DelayInput
          autoFocus
          onFocus={this.putCaretAtEnd}
          minLength={1}
          delayTimeout={300}
          onChange={this.handleChange}
          placeholder="Search"
          value={this.props.phrase}
        />
      </form>
    );
  }
}

export default AthleteSearch;