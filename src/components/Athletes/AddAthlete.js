import React from 'react';
import { connect } from 'react-redux';
import { toastSuccess, toastErrors } from '../../actions/toasts';
import { FormattedMessage, intlShape } from 'react-intl';
import 'react-toastify/dist/ReactToastify.css';
import addAthleteRequest from './requests/addAthlete';
import { addAthlete } from '../../actions/athletes';

class AddAthlete extends React.Component {
  state = {
    errors: []
  }

  showErrors = () => {
    if (this.state.errors.length > 0) { 
      toastErrors(this.state.errors);
    }
  }

  addNewAthlete = (e) => {
    e.preventDefault();

    const athlete = {
      firstName: e.target.elements.firstName.value,
      lastName: e.target.elements.lastName.value,
      genre: e.target.elements.genre.value,
      dateOfBirth: e.target.elements.dateOfBirth.value
    };

    addAthleteRequest(athlete).then((res) => {
      let errs = [];
      if (res.errors) {
        errs = res.errors;
      } else {
        errs = [];
      }

      this.setState(() => ({
        errors: errs
      }));

      return res;
    }).then(res => {
      if (this.state.errors.length === 0 ) {
        this.props.addAthlete({
          ...athlete,
          id: res.id,
        });

        toastSuccess("The athlete has been created");
        this.props.history.push('/');
      }
    });
  }

  componentDidUpdate() {
    this.showErrors();
  }

  render() {
    return(
      <div className="athletes__add-athlete">
        <div className="box-layout">
          <h2>
            <FormattedMessage
              id="Athlete.AddAthlete"
              defaultMessage="Add Athlete"
            />
          </h2>
          <div className="athletes__add-athlete-form">
            <form onSubmit={this.addNewAthlete} className="athletes__form">
              <FormattedMessage id="Athlete.FormFirstName" defaultMessage="First Name">
                {placeholder=>  
                  <input type="text" name="firstName" placeholder={placeholder} className="athletes__form-input" />
                }
              </FormattedMessage>

              <FormattedMessage id="Athlete.FormLastName" defaultMessage="Last Name">
                {placeholder=>  
                  <input type="text" name="lastName" placeholder={placeholder} className="athletes__form-input" />
                }
              </FormattedMessage>

              <select name="genre">
                <FormattedMessage id="Athlete.Male" defaultMessage="Male">
                  {
                    option => (
                      <option value="male">
                        {option}
                      </option>
                    )
                  }
                </FormattedMessage>
                <FormattedMessage id="Athlete.Female" defaultMessage="Female">
                  {
                    option => (
                      <option value="female">
                        {option}
                      </option>
                    )
                  }
                </FormattedMessage>
              </select>
              
              <FormattedMessage id="Athlete.FormDateOfBirth" defaultMessage="Date of Birth">
                {placeholder=>  
                  <input type="text" name="dateOfBirth" placeholder={placeholder} className="athletes__form-input" />
                }
              </FormattedMessage>
              
              <FormattedMessage id="Athlete.Save" defaultMessage="Save">
                {placeholder=>  
                  <input type="submit" value={placeholder} className="button wide_button submit_input" />
                }
              </FormattedMessage>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    addAthlete: athlete => dispatch(addAthlete(athlete))
  }
}

export default connect(undefined, mapDispatchToProps)(AddAthlete);