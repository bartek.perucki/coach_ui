import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollTable from '../Table/ScrollTable';
import AthletesTableRow from './AthletesTableRow';
import AthleteSearch from './AthleteSearch';
import getAthletesQuery from './requests/getAthletes';
import { setAthletes } from '../../actions/athletes';
import { getFilteredAthletes } from '../../selectors/athletes';
import { getSearchQuery } from '../../selectors/router';


class Athletes extends React.Component {
  headers = () => ([
    "Last Name",
    "First Name",
    "Genre",
    "Date of Birth",
    ""
  ]);

  fillRows = () => {
    let rows = [];
    const { athletes } = this.props;

    athletes.map((athlete) => {
      rows.push(
        <AthletesTableRow key={athlete.id} id={athlete.id} cells={[
          athlete.lastName,
          athlete.firstName,
          athlete.genre,
          athlete.dateOfBirth,
        ]} />
      );
    });

    return rows;
  };

  updateFilter = (phrase) => {
    this.props.history.push({
      search: phrase ? `?search=${phrase}` : ''
    });
  };

  componentDidMount() {
    getAthletesQuery().then((athletes) => {
      this.props.setAthletes(athletes);
    });
  };

  render() {
    return (
      <div className="dashboard">
        <div className="athletes">
          <div className="athletes__top-panel">
            <div className="athletes__button">
              <Link to="/athletes/new">
                <button className="button">New Athlete</button>
              </Link>
            </div>
            <AthleteSearch 
              updateFilter={this.updateFilter} 
              phrase={this.props.search}
            />
          </div>
          <ScrollTable 
            noResults={"No athletes found."}
            headers={this.headers()}
            contentClassPrefix={"athletes"}
          >
            { this.fillRows() }
          </ScrollTable>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    athletes: getFilteredAthletes(state, props),
    search: getSearchQuery(state, props),
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    setAthletes: athletes => dispatch(setAthletes(athletes)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Athletes);