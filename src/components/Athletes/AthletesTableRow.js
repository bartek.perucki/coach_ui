import React from 'react';
import { MdModeEdit } from 'react-icons/md';
import { Link } from 'react-router-dom';

class AthletesTableRow extends React.Component {
  fillCells = () => {
    let cells = [];

    this.props.cells.map((cell, index) => {
      cells.push(
        <td key={index} className={`athletes__cell-${index}`}>
          {cell}
        </td>
      );
    });

    cells.push(
      <td key={"edit"} className={`athletes__cell-4`}>
        <Link to={`/athletes/edit/${this.props.id}`}><MdModeEdit className="icon"/></Link>
      </td>
    );

    return cells;
  }

  render() {
    return(
      <tr>
        {this.fillCells()}
      </tr>
    )
  }
}

export default AthletesTableRow;