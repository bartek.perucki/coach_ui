import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import CalendarHeatmap from 'react-calendar-heatmap';
import TrainingTile from '../Trainings/TrainingTile';
import NotFoundPage from '../NotFoundPage';
import { toastSuccess, toastErrors } from '../../actions/toasts';
import updateAthleteQuery from './requests/updateAthlete';
import deleteAthleteQuery from './requests/deleteAthlete';
import getAthleteQuery from './requests/getAthlete';
import { getAthleteAttendance, getTrainingsSinceAthleteCreated } from '../../selectors/attendance';
import { updateAthlete, deleteAthlete } from '../../actions/athletes';
import {
  getAthlete,
  getAthleteTrainings,
  getAthleteLastTraining,
  getAthleteTrainingsThisWeek,
  getAthleteTrainingsThisMonth
} from '../../selectors/athletes';
import { getTrainings } from '../../selectors/training/trainingList';
import 'react-calendar-heatmap/dist/styles.css';


class Athlete extends React.Component {
  recentTrainings = () => {
    return this.props.athleteTrainings.splice(-20);
  }

  percentage = () => {
    const { trainingsSinceAthlete, athleteTrainings } = this.props;

    if(trainingsSinceAthlete.length > 0) {
      return Math.round((athleteTrainings.length / trainingsSinceAthlete.length) * 100); 
    } else {
      return 0
    }
  }

  handleDetailsChange = e => {
    const { name, value } = e.target;
    const { athlete, updateAthlete } = this.props;
    let updates = {};
    
    switch(name) {
      case "firstName":
        updates = { firstName: value }
        break;
      case "lastName":
        updates = { lastName: value }
        break;
      case "dateOfBirth":
        updates = { dateOfBirth: value }
        break;
      case "genre":
        updates = { genre: value }
        break; 
    }

    updateAthlete(athlete.id, updates);
  }

  updateAthlete = () => {
    const { athlete } = this.props;
    const { id, firstName, lastName, genre, dateOfBirth } = athlete;

    updateAthleteQuery(id, {
      firstName,
      lastName,
      dateOfBirth,
      genre
    })
    .then((res) => {
      if (res.errors) {
        toastErrors([res.errors]);
      } else {
        toastSuccess("The athlete has been updated")
      }
    })
    .catch((res) => {
      toastErrors([res]);
    });
  }

  deleteAthlete = () => {
    const { athlete, deleteAthlete, history } = this.props;

    if(confirm('Are you sure you want to remove this athlete?')) {
      deleteAthleteQuery(athlete.id)
        .then(() => {
          deleteAthlete(athlete.id);
          toastSuccess("The athlete has been deleted")
          history.push('/attendance');
        })
        .catch((e) => {
          console.error(e);
          toastErrors(["Server error. Cannot delete the athlete."]);
        });
    } else {
      return false;
    }
  }

  onCalendarClick = value => {
    this.props.history.push(`/trainings/${value.date}`);
  }

  componentDidMount() {
    const athlete = getAthleteQuery(this.props.match.params.id);

    this.props.updateAthlete(athlete.id, {
      firstName: athlete.firstName,
      lastName: athlete.lastName,
      dateOfBirth: athlete.dateOfBirth,
      genre: athlete.genre
    });
  }

  render() {
    const { attendance, athlete, lastTraining, thisWeekTrainings, thisMonthTrainings } = this.props;
    const { firstName, lastName, dateOfBirth, genre, category, createdAt } = athlete;
    const endDate = moment().format("YYYY-MM-DD");
    const startDate = moment().subtract(250, 'days').format("YYYY-MM-DD");

    if(!athlete.id) {
      return(<NotFoundPage />);
    }

    return (
      <div className="athlete">
        <h2>Details</h2>
        <div className="athlete__details">
          <div className="athlete__details-personal">
            <label>
              <FormattedMessage
                id="Athlete.FirstName"
                defaultMessage="First Name"
              />
              <input type="text" value={firstName} name="firstName" onChange={this.handleDetailsChange}/>
            </label>
            <label>
              <FormattedMessage
                id="Athlete.LastName"
                defaultMessage="Last Name"
              />
              <input type="text" value={lastName} name="lastName" onChange={this.handleDetailsChange} />
            </label>
            <label>
              <FormattedMessage
                id="Athlete.DateOfBirth"
                defaultMessage="Date of Birth"
              />
              <input type="text" value={dateOfBirth} name="dateOfBirth" onChange={this.handleDetailsChange} />
            </label>
            <label>
              <FormattedMessage
                id="Athlete.Category"
                defaultMessage="Category"
              />
              <input type="text" value={category} disabled/>
            </label>
            <label>
              <FormattedMessage
                id="Athlete.Genre"
                defaultMessage="Genre"
              />
              <input type="text" value={genre} name="genre" onChange={this.handleDetailsChange} />
            </label>
            <label>
              <FormattedMessage
                id="Athlete.Joined"
                defaultMessage="Joined"
              />
              <input type="text" value={createdAt} disabled/>
            </label>
            <button className="button submit_input" onClick={this.updateAthlete}>
              <FormattedMessage
                id="Athlete.Update"
                defaultMessage="Update"
              />
            </button>

            <button className="link" onClick={this.deleteAthlete}>
              <FormattedMessage
                id="Athlete.Delete"
                defaultMessage="Delete Athlete"
              />
            </button>
          </div>

          <div className="athlete__details-results">
            
          </div>
        </div>

        <h2>
          <FormattedMessage
            id="Athlete.Attendance"
            defaultMessage="Attendance"
          />
        </h2>
        <div className="athlete__attendance">
          <div className="athlete__attendance-details">
            <span>
              <FormattedMessage
                id="Athlete.LastTraining"
                defaultMessage="Last Trainings"
              />: <b>
                {
                  lastTraining.date || (
                    <FormattedMessage
                      id="Athlete.Never"
                      defaultMessage="Never"
                    />
                  )
                }
              </b>
            </span>
            <span>
              <FormattedMessage
                id="Athlete.ThisWeek"
                defaultMessage="This Week"
              />: <b>{thisWeekTrainings.length}</b>
            </span>
            <span>
              <FormattedMessage
                id="Athlete.ThisMonth"
                defaultMessage="This Month"
              />: <b>{thisMonthTrainings.length}</b>
            </span>
            <span>
              <FormattedMessage
                id="Athlete.Total"
                defaultMessage="Total"
              />: <b>{this.percentage()}%</b>
            </span>
          </div>
          <div className="athlete__attendance-heatmap">
            {attendance && 
              <CalendarHeatmap 
                className="heatmap"
                startDate={startDate}
                endDate={endDate}
                values={attendance}
                onClick={this.onCalendarClick}
                classForValue={(value) => {
                  if (!value) {
                    return 'color-empty';
                  } else if (value.count === 0) {
                    return 'color-red';
                  }
                  return `color-green`;
                }}
              />
            }
          </div>
        </div>

        <h2>
          <FormattedMessage
            id="Athlete.RecentTrainings"
            defaultMessage="Recent Trainings"
          />
        </h2>
        <div className="athlete__trainings">
          {this.recentTrainings().map(training => (
            <TrainingTile training={training} search={`${firstName} ${lastName}`} key={training.id}/>
          ))}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    attendance: getAthleteAttendance(state, props),
    athlete: getAthlete(state, props),
    athleteTrainings: getAthleteTrainings(state, props),
    lastTraining: getAthleteLastTraining(state, props),
    thisWeekTrainings: getAthleteTrainingsThisWeek(state, props),
    thisMonthTrainings: getAthleteTrainingsThisMonth(state, props),
    trainings: getTrainings(state, props),
    trainingsSinceAthlete: getTrainingsSinceAthleteCreated(state, props),
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    updateAthlete: (id, updates) => dispatch(updateAthlete(id, updates)),
    deleteAthlete: id => dispatch(deleteAthlete(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Athlete);