import axios from 'axios';

export default (id) => {
  const url = `http://localhost:3001/api/v1/athlete/${id}`;
  const token = localStorage.getItem('jwt');

  return axios.delete(url, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }})
    .then((res) => {
      return res.data.msg;
    })
    .catch(() => {
      return { errors: [ "Cannot delete the athlete. API error." ] };
    });
}