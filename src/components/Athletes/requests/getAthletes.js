import axios from 'axios';
import moment from 'moment';

export default (filter) => {
  const url = "http://localhost:3001/api/v1/athletes";
    const token = localStorage.getItem('jwt');

    return axios.get(url, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-type': 'application/json'
      }
    }).then((res) => {
      let athletes = [];

      res.data.data.map((athlete) => {
        const { 
          id: id,
          first_name: firstName,
          last_name: lastName,
          genre: genre,
          date_of_birth: dateOfBirth,
          created_at: createdAt
        } = athlete;

        athletes.push({
          id,
          firstName,
          lastName,
          genre,
          dateOfBirth,
          createdAt: moment(createdAt).format("YYYY-MM-DD")
        });
      });

      return athletes;
    }).then((athletes) => {
      if (filter) {
        return athletes.filter((athlete) => {
          filter = filter.trim();
          
          const fullName = `${athlete.firstName} ${athlete.lastName}`;

          const firstNameMatch = athlete.firstName.toLowerCase().includes(filter.toLowerCase());
          const lastNameMatch = athlete.lastName.toLowerCase().includes(filter.toLowerCase());
          const fullNameMatch = fullName.toLowerCase().includes(filter.toLowerCase());
          const birthdayMatch = athlete.dateOfBirth.includes(filter);

          return firstNameMatch || lastNameMatch || fullNameMatch || birthdayMatch;
        })
      } else {
        return athletes;
      }
    });
}