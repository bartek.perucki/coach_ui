import axios from 'axios';

export default (id, athlete) => {
  const {
    firstName = '',
    lastName = '',
    genre = '',
    dateOfBirth = ''
  } = athlete;

  const request = {
    "athlete": {
      "first_name": firstName,
      "last_name": lastName,
      "genre": genre,
      "date_of_birth": dateOfBirth
    }
  };

  const url = `http://localhost:3001/api/v1/athlete/${id}`;
  const token = localStorage.getItem('jwt');

  return axios.put(url, request, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }
  })
    .then((res) => {
      if (res.data.status !== 200) {
        return {
          errors: res.data.msg
        }
      } else {
        return res.data.data;
      }
    })
    .catch((res) => {
      return { errors: [ "Cannot update the athlete. API error." ]};
    });
}