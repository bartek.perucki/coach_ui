import axios from 'axios';

export default (id) => {
  const url = `http://localhost:3001/api/v1/athlete/${id}`;
  const token = localStorage.getItem('jwt');

  return axios.get(url, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }
  }).then((res) => {
    if(res.data.status === 404) {
      return {};
    }

    const athleteData = res.data.data;

    const athlete = { 
      id: athleteData.id,
      firstName: athleteData.first_name,
      lastName: athleteData.last_name,
      genre: athleteData.genre,
      dateOfBirth: athleteData.date_of_birth
    };

    return athlete;
  });
}