import React from 'react';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { toastSuccess, toastErrors } from '../../actions/toasts';
import updateAthleteQuery from './requests/updateAthlete';
import deleteAthleteQuery from './requests/deleteAthlete';
import { updateAthlete, deleteAthlete } from '../../actions/athletes';

class EditAthlete extends React.Component {
  state = {
    athlete: {
      firstName: '',
      lastName: '',
      genre: '',
      dateOfBirth: ''
    },
    errors: []
  }

  showErrors = () => {
    if (this.state.errors.length > 0) { 
      toastErrors(this.state.errors);
    }
  }

  editAthlete = (e) => {
    e.preventDefault();

    const id = this.props.match.params.id;

    const athlete = {
      firstName: e.target.elements.firstName.value,
      lastName: e.target.elements.lastName.value,
      genre: e.target.elements.genre.value,
      dateOfBirth: e.target.elements.dateOfBirth.value
    };

    updateAthleteQuery(id, athlete)
      .then((res) => {
        if (res.errors) {
          this.setState(() => ({
            errors: res.errors
          }));
        }
      })
      .then(() => {
        if (this.state.errors.length === 0) {
          this.props.updateAthlete(id, athlete);
          toastSuccess("The athlete has been updated")
          this.props.history.push('/');
        }
      })
      .catch((res) => {
        this.setState(() => ({
          errors: res
        }));
      });
  }

  deleteAthlete = () => {
    const id = this.props.match.params.id;
    if(confirm('Are you sure you want to remove this athlete?')) {
      deleteAthleteQuery(id)
        .then(() => {
          this.props.deleteAthlete(id);
        })
        .then(() => {
          toastSuccess("The athlete has been deleted")
          this.props.history.push('/');
        });
    } else {
      return false;
    }
  }

  onFirstNameChange = (e) => {
    const athlete = this.state.athlete;
    athlete.firstName = e.target.value;

    this.setState(() => ({ 
      athlete,
      errors: []
    }));
  }

  onLastNameChange = (e) => {
    const athlete = this.state.athlete;
    athlete.lastName = e.target.value;

    this.setState(() => ({ 
      athlete,
      errors: []
    }));
  }

  onGenreChange = (e) => {
    const athlete = this.state.athlete;
    athlete.genre = e.target.value;

    this.setState(() => ({ 
      athlete,
      errors: []
    }));
  }

  onBirthdayChange = (e) => {
    const athlete = this.state.athlete;
    athlete.dateOfBirth = e.target.value;

    this.setState(() => ({ 
      athlete,
      errors: []
    }));
  }

  componentDidUpdate() {
    if (this.state.errors.length > 0) {
      this.showErrors();
    }
  }

  componentWillMount() {
    const { athlete } = this.props;
    this.setState(() => ({ athlete }));
  }

  render() {
    const { athlete } = this.props;

    return(
      <div className="athletes__add-athlete">
        <div className="box-layout">
          <h2 className="red red__border">Edit Athlete</h2>
          <div className="grey-padding">
            <form onSubmit={this.editAthlete} className="athletes__form">
              <input type="text" 
                    value={athlete.firstName} 
                    name="firstName" 
                    placeholder="First Name" 
                    className="athletes__form-input"
                    onChange={this.onFirstNameChange}
              />
              <input type="text" 
                    value={athlete.lastName} 
                    name="lastName"
                    placeholder="Last Name" 
                    className="athletes__form-input"
                    onChange={this.onLastNameChange}
              />
              <select name="genre" value={athlete.genre} onChange={this.onGenreChange}>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
              <input type="text" 
                    value={athlete.dateOfBirth}
                    name="dateOfBirth" 
                    placeholder="Date of Birth" 
                    className="athletes__form-input"
                    onChange={this.onBirthdayChange}
              />
              <input type="submit" value="Save" className="submit_input athletes__form-button"/>
            </form>
            <div className="athletes__edit-options">
              <button className="athletes__form-button primary_button" onClick={() => { this.props.history.push('/') }}>
                Cancel
              </button>
              <button className="athletes__form-button danger_button" onClick={this.deleteAthlete}>
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    athlete: state.athletes.find(athlete => athlete.id == props.match.params.id)
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    updateAthlete: (id, athlete) => dispatch(updateAthlete(id, athlete)),
    deleteAthlete: id => dispatch(deleteAthlete(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditAthlete);