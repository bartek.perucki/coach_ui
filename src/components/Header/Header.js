import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';
import { FormattedMessage } from 'react-intl';

export class Header extends React.Component {
  handleLogout = () => {
    localStorage.removeItem('jwt');
    this.props.logout();
  }

  checkActive = tab => {
    const activeTab = location.pathname.split('/')[1];
    return activeTab === tab && "header__link-active";
  }

  render() {
    return (
      <header className="header">
        <div className="content-container">
          <div className="header__content">
            <Link className="header__title" to="/athletes">
              <img src="/images/logo.svg" alt="Coach6" className="header__logo"></img>
            </Link>
            <div className="header__links">
              <Link className={`header__link ${this.checkActive('athletes')}`} to="/home">
                <FormattedMessage
                  id="Header.Home"
                  defaultMessage="Home"
                />
              </Link>
              <Link className={`header__link ${this.checkActive('trainings')}`} to="/trainings">
                <FormattedMessage
                  id="Header.Trainings"
                  defaultMessage="Trainings"
                />
              </Link>
              <Link className={`header__link ${this.checkActive('results')}`} to="/results">
                <FormattedMessage
                  id="Header.Results"
                  defaultMessage="Results"
                />
              </Link>
            </div>
            <button className="button button--link" onClick={this.handleLogout}>
              <FormattedMessage
                id="Header.Logout"
                defaultMessage="Logout"
              />
            </button>
          </div>
        </div>
      </header>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout())
});

export default connect(undefined, mapDispatchToProps)(Header);