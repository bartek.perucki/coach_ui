import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { MdSettings, MdHome, MdClear, MdInsertDriveFile, MdFitnessCenter } from 'react-icons/md';
import { logout } from '../../actions/auth';

class Panel extends React.Component {
  logout = () => {
    localStorage.removeItem('jwt');
    this.props.logout();
  }

  render() {
    return (
      <div className="panel">
        <div className="panel__content">
          <Link className="panel__title" to="/athletes">
            <h1>Coach App</h1>
          </Link>
          <Link className="panel__link" to="/athletes">
            <MdHome className="icon"/> Dashboard
          </Link>
          <Link className="panel__link" to={`/trainings/new/${moment().format('YYYY-MM-DD')}`}>
            <MdFitnessCenter className="icon"/> New Training
          </Link>
          <Link className="panel__link" to="/">
            <MdSettings className="icon"/> Settings
          </Link>
          <Link className="panel__link" to="/">
            <MdInsertDriveFile className="icon"/> About
          </Link>
          <button className="panel__button" onClick={this.logout}>
            <MdClear className="icon"/> Logout
          </button>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout())
});

export default connect(undefined, mapDispatchToProps)(Panel);