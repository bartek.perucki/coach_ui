import axios from 'axios';

export default () => {
  const url = "http://localhost:3001/api/v1/attendance";
  const token = localStorage.getItem('jwt');

  return axios.get(url, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }
  }).then(res => {
    return res.data;
  });
}