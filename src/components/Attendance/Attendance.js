import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import CalendarHeatmap from 'react-calendar-heatmap';
import { setAttendance } from '../../actions/attendance';
import { getFilteredAthletes } from '../../selectors/athletes';
import { getTrainings } from '../../selectors/training/trainingList';
import { getAttendance } from '../../selectors/attendance';
import getAthletesAttendanceQuery from './requests/getAthletesAttendance';
import AttendanceCard from './AttendanceCard';
import 'react-calendar-heatmap/dist/styles.css';


class Attendance extends React.Component {

  trainingDates = () => {
    return this.props.trainings.map(training => ({ date: training.date }));
  }

  onCalendarClick = value => {
    this.props.history.push(`/trainings/${value.date}`);
  }

  onMouseOver = (event, value) => {
    if(value) {
      // ToDo: Show tooltip
      // console.log(value);
    }
  }

  componentDidMount() {
    getAthletesAttendanceQuery().then(attendance => {
      this.props.setAttendance(attendance);
    });
  }

  render() {
    const endDate = moment().format("YYYY-MM-DD");
    const startDate = moment().subtract(250, 'days').format("YYYY-MM-DD")

    return(
      <div className="attendance">
        <div className="attendance__total">
          <h2>
            <FormattedMessage
              id="Attendance.TotalTrainings"
              defaultMessage="Total Trainings"
            />
          </h2>
          
          <CalendarHeatmap 
            startDate={new Date(startDate)}
            endDate={new Date(endDate)}
            values={this.trainingDates()}
            onClick={this.onCalendarClick}
            onMouseOver={this.onMouseOver}
            classForValue={(value) => {
              if (!value) {
                return 'color-empty';
              }
              return `color-green`;
            }}
          />
          <div className="attendance__total-description">
            <div className="attendance__total-description-training">
              <span className="square">&#9632;</span> - <FormattedMessage 
                id="Attendance.DescriptionTrainings"
                defaultMessage="training"
              />
            </div>
            <div className="attendance__total-description-absence">
              <span className="square">&#9632;</span> - <FormattedMessage
                id="Attendance.DescriptionAbsence"
                defaultMessage="absence"
              />
            </div>
            <div className="attendance__total-description-competition">
              <span className="square">&#9632;</span> - <FormattedMessage
                id="Attendance.DescriptionCompetition"
                defaultMessage="competition"
              />
            </div>
          </div>
        </div>
        <div className="attendees">
          <div className="attendees__header">
            <h2>
              <FormattedMessage
                id="Attendance.Athletes"
                defaultMessage="Athletes"
              />
            </h2>
            <Link to="/athletes/new" className="button submit_input">
              <FormattedMessage
                id="Attendance.AddNewAthlete"
                defaultMessage="Add New Athlete"
              />
            </Link>
          </div>
          { this.props.athletes.map(athlete => (
            <AttendanceCard
              attendance={this.props.attendance}
              athlete={athlete}
              startDate={startDate}
              endDate={endDate}
              key={athlete.id} />
          ))}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    athletes: getFilteredAthletes(state, props),
    trainings: getTrainings(state, props),
    attendance: getAttendance(state, props)
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    setAttendance: attendance => dispatch(setAttendance(attendance))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Attendance);