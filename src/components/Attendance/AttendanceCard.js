import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import CalendarHeatmap from 'react-calendar-heatmap';
import {
  getAthleteTrainings,
  getAthleteTrainingsThisWeek,
  getAthleteTrainingsThisMonth
} from '../../selectors/athletes';
import {
  getTrainingsThisWeekCount,
  getTrainingsThisMonthCount
} from '../../selectors/training/trainingList';
import 'react-calendar-heatmap/dist/styles.css';

class AttendanceCard extends React.Component {
  lastSeen = () => {
    const attendance = this.attendance();

    if(attendance.length > 0) {
      const last = attendance.reduce((prev, current) => {
        return (moment(current.date).isAfter(moment(prev.date)) && current.count > 0 ? current : prev)
      })
      
      return last.date;
    } else {
      return <FormattedMessage
        id="AttendanceCard.LastSeenNever"
        defaultMessage="Never"
      />;
    }
  }

  thisWeek = () => {
    const { athleteTrainingsThisWeek, trainingsThisWeekCount } = this.props;
    return `${athleteTrainingsThisWeek.length}/${trainingsThisWeekCount}`;
  }

  thisMonth = () => {
    const { athleteTrainingsThisMonth, trainingsThisMonthCount } = this.props;
    return `${athleteTrainingsThisMonth.length}/${trainingsThisMonthCount}`;
  }

  attendance = () => {
    const { attendance, athlete } = this.props;
    return attendance[athlete.id];
  }

  render(){
    const { athlete, startDate, endDate } = this.props;
    const attendance = this.attendance();

    return(
      <div className="attendees__box">
        <Link className="attendees__box-name" to={`/athletes/${athlete.id}`}>
          {`${athlete.lastName} ${athlete.firstName}`}
        </Link>
        <div className="attendees__box-heatmap">
          { attendance ?
            <CalendarHeatmap 
              className="heatmap"
              startDate={startDate}
              endDate={endDate}
              values={attendance}
              classForValue={(value) => {
                if (!value) {
                  return 'color-empty';
                } else if (value.count === 0) {
                  return 'color-red';
                }
                return `color-green`;
              }}
            />
          :
            <img src="/images/loader.gif" className="attendees__box-loading"/>
          }
        </div>
        { attendance &&
          <div className="attendees__box-details">
            <small>
            <FormattedMessage
              id="AttendanceCard.LastSeen"
              defaultMessage="Last Seen"
            />: <b>{this.lastSeen()}</b>
            </small>
            <small>
            <FormattedMessage
              id="AttendanceCard.ThisWeek"
              defaultMessage="This Week"
            />: <b>{this.thisWeek()}</b>
            </small>
            <small>
            <FormattedMessage
              id="AttendanceCard.ThisMonth"
              defaultMessage="This Month"
            />: <b>{this.thisMonth()}</b>
            </small>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    athleteTrainings: getAthleteTrainings(state, props),
    athleteTrainingsThisWeek: getAthleteTrainingsThisWeek(state, props),
    athleteTrainingsThisMonth: getAthleteTrainingsThisMonth(state, props),
    trainingsThisWeekCount: getTrainingsThisWeekCount(state, props),
    trainingsThisMonthCount: getTrainingsThisMonthCount(state, props),
  }
}

export default connect(mapStateToProps)(AttendanceCard);