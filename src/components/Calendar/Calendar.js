import React from "react";
import moment from "moment";
import { MdChevronLeft, MdChevronRight } from 'react-icons/md';

class Calendar extends React.Component {
  state = {
    currentMonth: new Date(),
    selectedDate: new Date()
  }

  renderHeader = () => {
    const dateFormat = "MMMM YYYY";

    return (
      <div className="header">
        <MdChevronLeft 
          className="icon"
          onClick={this.prevMonth}
        />

        <span>
          {moment(this.state.currentMonth).format(dateFormat)}
        </span>
        
        <MdChevronRight
          className="icon"
          onClick={this.nextMonth}
        />
      </div>
    );
  }

  renderDays = () => {
    const dateFormat = "dddd";
    const days = [];

    let startDate = moment(this.state.currentMonth).startOf('week').add(1, 'day');

    for (let i = 0; i < 7; i++) {
      days.push(
        <div className="col col-center" key={i}>
          {moment(startDate).add(i, 'd').format(dateFormat)}
        </div>
      );
    }

    return <div className="days row">{days}</div>;
  }

  renderCells = () => {
    const { currentMonth, selectedDate } = this.state;
    const monthStart = moment(currentMonth).startOf('month');
    const monthEnd = moment(currentMonth).endOf('month');
    const startDate = moment(monthStart).startOf('week').add(1, 'day');
    const endDate = moment(monthEnd).endOf('week').add(1, 'day');

    const dateFormat = "D";
    const rows = [];

    let days = [];
    let day = startDate;
    let formattedDate = "";

    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = moment(day).format(dateFormat);
        const cloneDay = day;

        days.push(
          // ToDo: REFACTOR THIS TERNARY!!!!
          <div className="col cell-wrapper" key={day}>
            <div
              className={`cell ${
                !moment(day).isSame(monthStart, 'month')
                  ? "disabled"
                  : moment(day).isSame(selectedDate, 'day') ? "selected" : ""
              } ${moment().isSame(day, 'd') && "today"}`}
              key={day}
              onClick={() => this.onDateClick(moment(cloneDay))}
            >
              <span className="number">{formattedDate}</span>
              { moment(day).isSame(monthStart, 'month') && this.addCellOptions(day) }
            </div>
          </div>
        );
        day = moment(day).add(1, 'd');
      }
      rows.push(
        <div className="row" key={day}>
          {days}
        </div>
      );
      days = [];
    }

    return <div className="body">{rows}</div>;
  }

  onDateClick = day => {
    this.setState({
      selectedDate: day
    });
  };

  nextMonth = () => {
    this.setState((prevState) => ({
      currentMonth: moment(prevState.currentMonth).add(1, 'M')
    }));
  };

  prevMonth = () => {
    this.setState((prevState) => ({
      currentMonth: moment(prevState.currentMonth).subtract(1, 'M')
    }));
  };

  addCellOptions = (date) => {
    return this.props.addCellOptions(date);
  }

  render() {
    return (
      <div className="calendar">
        { this.renderHeader() }
        { this.renderDays() }
        { this.renderCells() }
      </div>
    );
  }
}

export default Calendar;