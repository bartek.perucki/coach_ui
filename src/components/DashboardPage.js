import React from 'react';
import DashboardHeader from './DashboardHeader';

// ToDo: Consider removal
class DashboardPage extends React.Component {
  render() {
    return(
      <div className="dashboard">
        <DashboardHeader updateOption={this.updateOption} active={this.state.activeOption} />
        { this.props.children }
      </div>
    )
  }
};

export default DashboardPage;