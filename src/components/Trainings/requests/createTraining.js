import axios from 'axios';

export default (training) => {
  const { date, category, name, hour } = training;

  const request = {
    'training': {
      'date': date,
      'category': category,
      'hour': hour,
      'name': name
    }
  };

  const url = "http://localhost:3001/api/v1/trainings";
  const token = localStorage.getItem('jwt');

  return axios.post(url, request, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }
  })
  .then((res) => {
    if (res.data.status !== 200) {
      return ({
        errors: res.data.msg
      });
    } else {
      return res.data.data;
    }
  })
  .catch(() => {
    return {
      errors: [
        "Cannot create a training. API error."
      ]
    }
  });
}