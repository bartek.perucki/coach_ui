import axios from 'axios';

export default (training_id) => {
  const url = "http://localhost:3001/api/v1/training_groups";
    const token = localStorage.getItem('jwt');

    return axios.get(url, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-type': 'application/json'
      },
      params: {
        training_id
      }
    }).then((res) => {
      let groups = [];

      res.data.map(group => {
        const {
          id,
          name,
          athletes,
          training_description: trainingDescription
        } = group;

        const basicAthletes = athletes.map(athlete => ({
          firstName: athlete.first_name,
          lastName: athlete.last_name,
          id: athlete.id
        }));

        groups.push({
          id,
          name,
          trainingDescription,
          athletes: basicAthletes
        });
      });

      return groups;
    });
}