import axios from 'axios';

export default (filter) => {
  const url = "http://localhost:3001/api/v1/trainings";
    const token = localStorage.getItem('jwt');

    return axios.get(url, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-type': 'application/json'
      }
    }).then((res) => {
      let trainings = [];

      res.data.map((training) => {
        const { 
          id: id,
          name: name,
          category: category,
          date: date,
          hour: hour,
          groups_number: groupsNumber,
          athletes: athletes
        } = training;

        const basicAthletes = athletes.map(athlete => ({
          firstName: athlete.first_name,
          lastName: athlete.last_name,
          id: athlete.id
        }));

        trainings.push({
          id,
          name,
          category,
          date,
          hour,
          groupsNumber,
          athletes: basicAthletes,
        });
      });

      return trainings;
    });
}