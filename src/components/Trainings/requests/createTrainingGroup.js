import axios from 'axios';

export default (trainingId, training_group) => {
  const { trainingDescription, athletes } = training_group;

  const request = {
    'training_group': {
      'training_id': trainingId,
      'training_description': trainingDescription,
      'athletes_attributes': athletes.map(id => ({'id': id}))
    }
  };

  const url = "http://localhost:3001/api/v1/training_groups";
  const token = localStorage.getItem('jwt');

  return axios.post(url, request, {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-type': 'application/json'
    }
  })
  .then((res) => {
    if (res.data.status !== 200) {
      return ({
        errors: res.data.msg
      });
    } else {
      return res.data.data;
    }
  })
  .catch(() => {
    return {
      errors: [
        "Cannot create a training group. API error."
      ]
    }
  });
}