import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { getTraining } from '../../selectors/training/trainingList';
import getTrainingGroupsRequest from './requests/getTrainingGroups';
import Description from './TrainingDescription';
import { toastErrors } from '../../actions/toasts';

class Training extends React.Component {
  state = {
    groups: [],
    search: ""
  }

  componentDidMount() {
    const training_id = this.props.match.params.id;

    getTrainingGroupsRequest(training_id).then(res => {
      this.setState({ groups: res });
    }).catch(() => {
      toastErrors(["Cannot find this training"]);
      this.props.history.push('/trainings');
    });

    const params = new URLSearchParams(this.props.location.search);
    const search = params.get('search');
    this.setState({ search });
  }

  render() {
    const { training } = this.props;
    const { groups, search } = this.state;
    const category = training ? training.category : "-";
    const date = training ? training.date : "-";
    const name = training ? training.name : "";
    const hour = training && training.hour;

    return(
      <div className="training">
        <h2 className="training__header">
          { name }
        </h2>
        <label className="training__label">
          <FormattedMessage
            id="Training.Category"
            defaultMessage="Category:"
          /> { category }
        </label>
        <label className="training__label">
          <FormattedMessage
            id="Training.Date"
            defaultMessage="Date:"
          /> { date }
        </label>
        {
          hour && (
            <label className="training__label">
              <FormattedMessage
                id="Training.Hour"
                defaultMessage="Hour:"
              /> { hour }
            </label>
          )
        }
        {
          groups.map((group, index) => (
            <Description group={group} index={index} search={search} key={index}/>
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    training: getTraining(state, props)
  }
}

export default connect(mapStateToProps)(Training);
