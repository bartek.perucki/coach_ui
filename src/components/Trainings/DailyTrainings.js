import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { getTrainingsFromDate } from '../../selectors/training/trainingList';
import TrainingTile from './TrainingTile';
import NotFoundPage from '../NotFoundPage';

class DailyTrainings extends React.Component {
  render() {
    const { trainings } = this.props;
    const count = trainings.length

    if(count === 0) {
      return(<NotFoundPage />);
    }

    return(
      <div className="trainings__daily">
        <h2>
          <FormattedMessage
            id="DailyTrainings.Header1"
            defaultMessage="You've got "
          />
          {count}
          <FormattedMessage
            id="DailyTrainings.Header2"
            defaultMessage=" training(s) on "
          />
          {trainings[0].date}
        </h2>
        <div className="trainings__daily-wrapper">
          { trainings.map(training => (
            <TrainingTile
              training={training}
              key={training.id}
            />
          ))}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    trainings: getTrainingsFromDate(state, props),
  }
}

export default connect(mapStateToProps)(DailyTrainings);
