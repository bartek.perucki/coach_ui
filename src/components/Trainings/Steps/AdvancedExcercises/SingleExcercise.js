import React from 'react';

class SingleExcercise extends React.Component {
  state = {
    description: "",
    finished: false
  }

  updateDescription = e => {
    const description = e.target.value;
    this.setState(() => ({ description }));
  } 

  save = () => {
    this.props.saveExcercise({
      id: this.props.excercise.id,
      type: "single",
      description: this.state.description,
      series: [],
    });
    this.setState(() => ({ finished: true }));
  }

  remove = () => {
    this.props.removeExcercise(this.props.excercise.id);
  }

  componentDidMount() {
    const { description, newRecord } = this.props.excercise;

    if(!newRecord) {
      this.setState(() => ({ 
        finished: true,
        description: description
      }))
    }
  }

  render() {
    const { description, finished } = this.state;

    return (
      <div>
        { finished ? (
          <div>
            <input disabled type="text" value={description} />
            <button onClick={this.remove}>Remove</button>
          </div>
        ) : (
          <div>
            <input type="text" placeholder="Description" value={description} onChange={this.updateDescription}/>
            <button onClick={this.save}>Save</button>
          </div>
        )}
      </div>
    )
  }
}

export default SingleExcercise;