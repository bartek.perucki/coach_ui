import React from 'react';
import { MdAddCircle } from 'react-icons/md';
import NewExcercise from './NewExcercise';

class AdvancedExcercise extends React.Component {
  state = {
    newExcercise: {}
  }

  setType = (type) => {
    this.setState((prevState) => ({
      newExcercise: {
        ...prevState.excercise,
        type
      }
    }));
  }

  addExcercise = () => {
    const excercise = {
      id: this.state.excercises.length,
      type: "single",
      description: "",
      series: [],
      newRecord: true,
    };

    this.setState(() => ({
      newExcercise: excercise
    }))
  }

  saveExcercise = excercise => {
    const {currentGroup} = this.props;
    this.props.addExcerciseToGroup(currentGroup, excercise);
  }

  removeExcercise = excerciseId => {
    const {currentGroup} = this.props;
    this.props.removeExcerciseFromGroup(currentGroup, excerciseId);
  }

  componentDidMount() {
    const { groups, currentGroup } = this.props;
    const group = groups[currentGroup];
    const excercises = group.excercises || [];

    this.setState(() => ({ excercises }));
  }

  render() {
    const { groups, currentGroup } = this.props;
    const group = groups[currentGroup];
    const excercises = (group.excercises || []).concat([this.state.newExcercise]);

    return(
      <div>
        <div>
          {excercises.map(excercise => (
            <NewExcercise
              key={excercise.id}
              excercise={excercise}
              setType={this.setType}
              saveExcercise={this.saveExcercise}
              removeExcercise={this.removeExcercise}
            />
          ))}
        </div>

        <button onClick={this.addExcercise}>
          <MdAddCircle className="icon"/>
          Add Excercise
        </button>
      </div>
    )
  }
}

export default AdvancedExcercise;