import React from 'react';
import SingleExcercise from './SingleExcercise';
import SeriesExcercise from './SeriesExcercise';

class NewExcercise extends React.Component {
  setType = e => {
    const { setType } = this.props;
    setType(e.target.value);
  }

  render() {
    const {excercise, saveExcercise, removeExcercise} = this.props;
    const {newRecord, type} = excercise;

    return(
      <div>
        <div>
          { newRecord && (
            <div>
              <label>
                <input 
                  type="radio"
                  checked={type === "single"}
                  onChange={this.setType}
                  value={"single"}
                />
                Single
              </label>
              <label>
                <input 
                  type="radio" 
                  checked={type === "series"} 
                  onChange={this.setType}
                  value={"series"}
                />
                Series
              </label>
            </div>
          )}
          
          {
            type === "single" && (
              <SingleExcercise 
                saveExcercise={saveExcercise} 
                removeExcercise={removeExcercise} 
                excercise={excercise}
              />
            )
          }

          {
            type === "series" && (
              <SeriesExcercise
                saveExcercise={saveExcercise} 
                excercise={excercise}
              />
            )  
          }
        </div>
      </div>
    )
  }
}

export default NewExcercise;