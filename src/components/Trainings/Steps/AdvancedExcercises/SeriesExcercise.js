import React from 'react';

class SeriesExcercise extends React.Component {
  state = {
    series: [
      { description: "", limit: "" }
    ]
  }

  addRow = () => {
    this.setState(prevState => ({
      series: [ ...prevState.series, { description: "", limit: "" } ]
    }));
  }

  removeLastRow = () => {
    this.setState(prevState => ({
      series: prevState.series.slice(0, -1)
    }));
  }

  duplicateLastRow = () => {
    this.setState(prevState => ({
      series: [ ...prevState.series, prevState.series[prevState.series.length - 1] ]
    }));
  }

  save = () => {
    
  }

  render() {
    // console.log(this.state.series);
    return (
      <div>
        <div>
          <input type="text" placeholder="Description" />
          <button onClick={this.save}>Save</button>
        </div>
        <div>
          { this.state.series.map((row, index) => (
            <div key={index}>
              <span>#{index + 1}</span>
              <input name={`description_${index}`} type="text" />
              <input name={`limit_${index}`} type="text" />
            </div>
          ))}
          <button onClick={this.addRow}>Add Row</button>
          <button onClick={this.duplicateLastRow}>Duplicate Last Row</button>
          <button onClick={this.removeLastRow}>Remove Last Row</button>
        </div>
      </div>
    )
  }
}

export default SeriesExcercise;