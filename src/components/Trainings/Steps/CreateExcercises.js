import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import BasicExcercises from './BasicExcercises/BasicExcercises';
import { getTrainingGroupsWithAthletes, getCurrentGroup } from '../../../selectors/training/createTraining';
import {
  setCurrentGroup
} from '../../../actions/training/createTraining';


class CreateExcercises extends React.Component {
  state = {
    currentGroup: 0,
    excercises: [],
    descriptions: [],
  }

  handleGroupChange = e => {
    const currentGroup = parseInt(e.target.value, 10);
    this.props.setCurrentGroup(currentGroup);
  }

  incrementGroup = () => {
    const { currentGroup, setCurrentGroup } = this.props;
    setCurrentGroup(currentGroup + 1);
  }

  decrementGroup = () => {
    const { currentGroup, setCurrentGroup } = this.props;
    setCurrentGroup(currentGroup - 1);
  }

  isGroupFirst = () => {
    const { currentGroup } = this.props;
    return currentGroup === 0; 
  }

  isGroupLast = () => {
    const { currentGroup, groups } = this.props;
    return currentGroup === (groups.length - 1); 
  }

  setDescription = (index, description) => {
    this.setState(prevState => ({
      descriptions: prevState.descriptions.map((desc, i) => {
        if(i === index) {
          return description;
        } else {
          return desc;
        }
      })
    }))
  }

  componentDidMount() {
    const { groups, currentGroup } = this.props;
    const group = groups[currentGroup];
    const excercises = group.excercises || [];

    this.setState(() => ({ 
      descriptions: Array(groups.length).fill(""),
      currentGroup: 0,
      excercises
    }));

    this.props.setCurrentGroup(0);
  }

  componentDidUpdate() {
    if(!this.state.descriptions.includes("")) {
      this.props.setReadyToSave(this.state.descriptions);
    } else {
      this.props.setNotReadyToSave(this.state.descriptions);
    }
  }

  render() {
    const {
      currentGroup,
      groups
    } = this.props;

    const group = groups[currentGroup];

    return(
      <div>
        <div className="trainings__new-excercises-groups">
          <select name="group" onChange={this.handleGroupChange} value={currentGroup}>
            {groups.map((group, index) => (
              <FormattedMessage id="NewTrainingExcercises.Group" defaultMessage="Group " key={index}>
                { placeholder => (
                    <option 
                      value={index} 
                      key={index}
                    >
                      {`${placeholder} #${index + 1} (${group.athletes.length})`}
                    </option>
                  )
                }
              </FormattedMessage>
            ))}
          </select>
        </div>

        <div className="trainings__new-excercises-athletes">
          {group.athletes.map(athlete => (
            <span key={athlete.id}>{`${athlete.firstName} ${athlete.lastName}, `}</span>
          ))}
        </div>

        <BasicExcercises
          group={group}
          groupsCount={groups.length}
          currentGroup={currentGroup}
          setDescription={this.setDescription}
          description={this.state.descriptions[currentGroup]}
          incrementGroup={this.incrementGroup}
          decrementGroup={this.decrementGroup}
        />
        <div className="trainings__new-excercises-navigation">
          <button onClick={this.decrementGroup} disabled={this.isGroupFirst()} className="button submit_input">
            <FormattedMessage
              id="NewTrainingExcercises.PreviousGroup"
              defaultMessage="Previous Group"
            />
          </button>

          <button onClick={this.incrementGroup} disabled={this.isGroupLast()} className="button submit_input">
            <FormattedMessage
              id="NewTrainingExcercises.NextGroup"
              defaultMessage="Next Group"
            />
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    groups: getTrainingGroupsWithAthletes(state, props),
    currentGroup: getCurrentGroup(state, props),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setCurrentGroup: currentGroup => dispatch(setCurrentGroup(currentGroup)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateExcercises);