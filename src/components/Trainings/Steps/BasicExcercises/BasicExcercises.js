import React from 'react';
import { FormattedMessage } from 'react-intl';

class BasicExcercises extends React.Component {
  handleDescriptionChange = e => {
    const { currentGroup, setDescription } = this.props;
    setDescription(currentGroup, e.target.value);
  }

  render() {
    const { description } = this.props;

    return(
      <div className="trainings__new-excercises-description">
        <label>
          <FormattedMessage
            id="NewTrainingExcercises.Description"
            defaultMessage="Description:"
          />
          <textarea value={description} onChange={this.handleDescriptionChange} />
        </label>
      </div>
    )
  }
}

export default BasicExcercises;