import React from 'react';

class NewTrainingSteps extends React.Component {
  stepComponent = () => {
    const { currentStep, children } = this.props;
    return children[currentStep];
  }

  render() {
    return(
      <div>
        { this.stepComponent() }
      </div>
    )
  }
}

export default NewTrainingSteps;