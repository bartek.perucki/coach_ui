import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { addTrainingGroup, resetTrainingGroups } from '../../../actions/training/createTraining';
import { getAthletesWithNoGroup, getTrainingGroupsWithAthletes } from '../../../selectors/training/createTraining';
import ScrollTable from '../../Table/ScrollTable';

class CreateGroups extends React.Component {
  state = {
    group: []
  }

  removeOrAddToGroup = (e) => {
    const id = parseInt(e.target.value, 10);

    if(this.state.group.includes(id)) {
      this.setState((prevState) => ({
        group: prevState.group.filter(i => i !== id)
      }))
    } else {
      this.setState((prevState) => ({
        group: [ ...prevState.group, id ]
      }))
    }
  }

  setGroup = () => {
    if(this.state.group.length) {
      this.props.addTrainingGroup({
        athletes: this.state.group,
        excercises: []
      });
      this.setState(() => ({ group: [] }));
    }
  }

  resetGroups = () => {
    this.setState(() => ({ group: [] }));
    this.props.resetTrainingGroups();
  }

  render() {
    const { athletes, groups } = this.props;

    return(
      <div className="trainings__new-groups">
      <div className="trainings__new-buttons">
        <button onClick={this.setGroup} className="button submit_input">
          <FormattedMessage
            id="NewTrainingGroups.SetGroup"
            defaultMessage="Set Group"
          />
        </button>
        <button onClick={this.resetGroups} className="button danger_button">
          <FormattedMessage
            id="NewTrainingGroups.ResetGroups"
            defaultMessage="Reset Groups"
          />
        </button>
      </div>
        <div  className="trainings__new-groups-creator">
          <div className="trainings__new-groups-athletes">
          <FormattedMessage id="NewTrainingGroups.AthletesHeader" defaultMessage="Athletes" >
              {
                header => (
                <ScrollTable headers={[header]} contentClassPrefix="trainings__new-groups">
                  { athletes.map(athlete => (
                    <tr key={athlete.id}>
                      <td>
                        <label>
                          <input
                            type="checkbox"
                            onChange={this.removeOrAddToGroup}
                            value={athlete.id}
                            name="Yes"
                            checked={this.state.group.includes(athlete.id)}
                          />
                          { ` ${athlete.firstName} ${athlete.lastName}` }
                        </label>
                      </td>
                    </tr>
                  ))}
                </ScrollTable>
                )
              }
            </FormattedMessage>
          </div>
          <div className="trainings__new-groups-groups">
            <FormattedMessage id="NewTrainingGroups.GroupsHeader" defaultMessage="Groups" >
              {
                header => (
                  <ScrollTable headers={[header]}>
                    { groups.map((group, index) => (
                        <tr key={index}>
                          <td>
                          <FormattedMessage
                            id="NewTrainingGroups.Group"
                            defaultMessage="Group "
                          />{`#${index + 1} (${group.athletes.length})`}
                          </td>
                        </tr>
                      )
                    )}
                    { groups.length === 0 && 
                        <tr>
                          <td className="trainings__new-groups-setup">
                            <FormattedMessage
                              id="NewTrainingGroups.EmptyMessage1"
                              defaultMessage="Setup your first group."
                            />
                            <br/>
                            <FormattedMessage
                              id="NewTrainingGroups.EmptyMessage2"
                              defaultMessage="In order to set up, select athletes"
                            />
                            <br/>
                            <FormattedMessage
                              id="NewTrainingGroups.EmptyMessage3"
                              defaultMessage="and click on"
                            />
                            <b><FormattedMessage
                              id="NewTrainingGroups.EmptyMessage4"
                              defaultMessage=" Set Group"
                            /></b>
                            <FormattedMessage
                              id="NewTrainingGroups.EmptyMessage5"
                              defaultMessage=" button"
                            />
                            
                          </td>
                        </tr>
                    }
                  </ScrollTable>
                )
              }
            </FormattedMessage>
          </div>
        </div>
        </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    athletes: getAthletesWithNoGroup(state, props),
    groups: getTrainingGroupsWithAthletes(state, props)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addTrainingGroup: group => dispatch(addTrainingGroup(group)),
    resetTrainingGroups: group => dispatch(resetTrainingGroups(group))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CreateGroups);