import React from 'react';
import { connect } from 'react-redux';
import { toastSuccess, toastErrors } from '../../actions/toasts';
import { FormattedMessage } from 'react-intl';
import CreateGroups from './Steps/CreateGroups';
import CreateExcercises from './Steps/CreateExcercises';
import NewTrainingSteps from './Steps/NewTrainingSteps';
import createTrainingGroup from './requests/createTrainingGroup';
import createTraining from './requests/createTraining';
import { getTraining, getCurrentStep } from '../../selectors/training/createTraining';
import { 
  setDate,
  setCategory,
  setName,
  setHour,
  incrementStep,
  decrementStep,
  resetStep,
  resetTraining,
  setExcercisesDescription
} from '../../actions/training/createTraining';


const NextStepButton = ({ currentStep, maxStep, incrementStep, saveTraining, isReadyToSave, training }) => {
  const onClick = currentStep !== maxStep ? incrementStep : saveTraining;
  const text = currentStep !== maxStep ? "Next" : "Save";
  const isDisabled = (currentStep === maxStep && !isReadyToSave) || (training.groups.length === 0);
  
  return (
    <button
      className={"button secondary_button wide_button"}
      onClick={onClick}
      disabled={isDisabled}
    >
      { currentStep !== maxStep ? (
        <FormattedMessage
          id="NewTraining.Next"
          defaultMessage="Next"
        />
      ) : (
        <FormattedMessage
          id="NewTraining.Save"
          defaultMessage="Save"
        />
      )}
    </button>
  )
}

const PreviousStepButton = ({ currentStep, decrementStep }) => {
  if( currentStep !== 0) {
    return <button className={"button secondary_button "} onClick={decrementStep}>
      <FormattedMessage
        id="NewTraining.Back"
        defaultMessage="Back"
      />
    </button>
  } else {
    return <button className={"button secondary_button "} disabled>
      <FormattedMessage
        id="NewTraining.Back"
      />
    </button>
  }
}

class NewTraining extends React.Component {
  state = {
    errors: [],
    isReadyToSave: false,
    trainingDescriptions: [],
    hour: "0000",
    hourLength: 0,
  }

  showErrors = () => {
    if (this.state.errors.length > 0) { 
      toastErrors(this.state.errors);
    }
  }


  onCategoryChange = (e) => {
    this.props.setTrainingCategory(e.target.value);
  }

  onHourChange = (e) => {
    this.props.setTrainingHour(e.target.value);
  }

  onNameChange = (e) => {
    this.props.setTrainingName(e.target.value);
  }

  setReadyToSave = trainingDescriptions => {
    this.setState({
      isReadyToSave: true,
      trainingDescriptions
    })
  }

  setNotReadyToSave = trainingDescriptions => {
    this.setState({
      isReadyToSave: false,
      trainingDescriptions
    })
  }

  saveTraining = () => {
    const training = this.props.training;
    const groups = training.groups.map((group, index) => ({
      ...group,
      trainingDescription: this.state.trainingDescriptions[index]
    }));
    
    createTraining(training).then(res => {
      const trainingId = res.id;
      let errors = [];
      let promises = [];

      if (res.errors) {
        this.setState(() => ({ errors: res.errors }));
        return null;
      }

      groups.forEach(group => {
        promises.push(createTrainingGroup(trainingId, group));
      });

      Promise.all(promises).then(responses => {
        responses.map(res => {
          errors = errors.concat(res.errors || []);
        })

        this.setState({ 
          errors: [...new Set(errors)]
        });
      }).then(() => {
        if (this.state.errors.length === 0 ) {
          toastSuccess("Training has been created");
          this.props.resetTraining();
          this.props.history.push('/trainings');
        } else {
          this.setState({ errors: [] });
        }
      });


    });
  }

  componentDidUpdate() {
    this.showErrors();
  }

  componentDidMount() {
    this.props.setTrainingDate(this.props.match.params.date);
  }

  componentWillUnmount() {
    // this.props.resetStep();
  }

  render() {
    const {
      training, 
      currentStep,
      incrementStep,
      decrementStep
    } = this.props;

    return(
      <div className="trainings__new-wizard">

        { currentStep === 0 && (
          <div className="trainings__new-top-panel">
            <select name="category" value={training.category} onChange={this.onCategoryChange} className="trainings__new-category">

              <FormattedMessage id="NewTraining.Stadium" defaultMessage="Stadium">
                {
                  option => (
                    <option value="stadium">
                      {option}
                    </option>
                  )
                }
              </FormattedMessage>

              <FormattedMessage id="NewTraining.Gym" defaultMessage="Gym">
                {
                  option => (
                    <option value="gym">
                      {option}
                    </option>
                  )
                }
              </FormattedMessage>

              <FormattedMessage id="NewTraining.Other" defaultMessage="Other">
                {
                  option => (
                    <option value="other">
                      {option}
                    </option>
                  )
                }
              </FormattedMessage>

            </select>
            <input
              className="trainings__new-hour"
              placeholder="00:00"
              onChange={this.onHourChange}
              value={training.hour}
            />
            <FormattedMessage id="NewTraining.TrainingName" defaultMessage="Training Name">
              {
                placeholder => (
                  <input
                    className="trainings__new-name"
                    placeholder={placeholder}
                    onChange={this.onNameChange}
                    value={training.name}
                  />
                )
              }
            </FormattedMessage>
          </div>
        )}

        <NewTrainingSteps currentStep={currentStep}>
          <CreateGroups />
          <CreateExcercises
            setReadyToSave={this.setReadyToSave}
            setNotReadyToSave={this.setNotReadyToSave}
          />
        </NewTrainingSteps>

        <div className={"trainings__new-footer"}>
          <div className={"trainings__new-footer-wrapper"}>
            <div className="trainings__new-footer-description">
              
              { currentStep === 0 ? (
                <p>
                  <FormattedMessage
                    id="NewTraining.StepOneDescription1"
                    defaultMessage="Athletes can be grouped. Then the same training can be set for a group of people."
                  />
                  <br/>
                  <FormattedMessage
                    id="NewTraining.StepOneDescription2"
                    defaultMessage="This way you will not have to duplicate descriptions."
                  />
                </p>
              ) : (
                <p>
                  <FormattedMessage
                    id="NewTraining.StepTwoDescription1"
                    defaultMessage="Please make sure that all of the groups have the description provided."
                  />
                  <br/>
                  <FormattedMessage
                    id="NewTraining.StepTwoDescription2"
                    defaultMessage="Once the description is ready, go to the next group, by clicking on the 'Next Group' button, or save the training."
                  />
                </p>
              )}
                
            </div>
            <div className="trainings__new-footer-buttons">
              <PreviousStepButton currentStep={currentStep} decrementStep={decrementStep} />

              <NextStepButton 
                currentStep={currentStep}
                incrementStep={incrementStep}
                maxStep={1}
                saveTraining={this.saveTraining}
                isReadyToSave={this.state.isReadyToSave}
                training={training}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    training: getTraining(state, props),
    currentStep: getCurrentStep(state, props)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    incrementStep: () => dispatch(incrementStep()),
    decrementStep: () => dispatch(decrementStep()),
    resetStep: () => dispatch(resetStep()),
    resetTraining: () => dispatch(resetTraining()),
    setTrainingDate: (date) => dispatch(setDate(date)),
    setTrainingName: (date) => dispatch(setName(date)),
    setTrainingHour: (date) => dispatch(setHour(date)),
    setTrainingCategory: (category) => dispatch(setCategory(category)),
    setExcercisesDescription: descriptions => setExcercisesDescription(dispatch(descriptions)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTraining);