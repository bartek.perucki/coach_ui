import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Trainings from './Trainings';
import NewTraining from './NewTraining';
import Training from './Training';
import DailyTrainings from './DailyTrainings';
import NotFoundPage from '../NotFoundPage';
import getTrainingsQuery from './requests/getTrainings';
import getAthletesQuery from '../Athletes/requests/getAthletes';
import { setTrainings, setLoadingState } from '../../actions/training/trainingList';
import { setAthletes } from '../../actions/athletes';

class TrainingsIndex extends React.Component {
  componentDidMount() {
    const { setLoadingState, setTrainings, setAthletes } = this.props;

    setLoadingState(true);

    getTrainingsQuery().then((trainings) => {
      setTrainings(trainings);
      setLoadingState(false);
    });

    getAthletesQuery().then((athletes) => {
      setAthletes(athletes);
    });
  }
  
  render(){
    return(
      <Switch>
        <Route path="/trainings/new/:date" component={NewTraining} exact={true} />
        <Route path="/trainings/:date" component={DailyTrainings} exact={true} />
        <Route path="/trainings/:date/:id" component={Training} exact={true} />
        <Route path="/trainings" component={Trainings} exact={true} />
        <Route component={NotFoundPage} />
      </Switch>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    setAthletes: athletes => dispatch(setAthletes(athletes)),
    setTrainings: trainings => dispatch(setTrainings(trainings)),
    setLoadingState: isLoading => dispatch(setLoadingState(isLoading)),
  }
}

export default connect(undefined, mapDispatchToProps)(TrainingsIndex);