import React from 'react';
import { FormattedMessage } from 'react-intl';

export default ({ group, index, search }) => (
  <div key={group.id} className="training__description">
    <FormattedMessage
      id="TrainingDescription.Group"
      defaultMessage="Group"
    >
      {
        placeholder => (
          <h4>{group.name || `${placeholder} #${index}`}</h4>
        )
      }
    </FormattedMessage>
    <span>
      {
        group.athletes.map(athlete => {
          if(`${athlete.firstName} ${athlete.lastName}` === search){
            return (
              <span><b style={{color: "#00b894"}}>{`${athlete.lastName} ${athlete.firstName}`}</b>, </span>
            );
          } else {
            return `${athlete.lastName} ${athlete.firstName}, `;
          }
        })
      }
    </span>
    <p style={{ whiteSpace: 'pre-wrap' }}>{group.trainingDescription}</p>
  </div>
)