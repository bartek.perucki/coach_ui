import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { MdAddBox, MdFitnessCenter } from 'react-icons/md';
import { Link } from 'react-router-dom';
import Calendar from '../Calendar/Calendar';
import getTrainingsRequest from './requests/getTrainings';
import { getTrainings } from '../../selectors/training/trainingList';
import { setTrainings } from '../../actions/training/trainingList';

class Trainings extends React.Component {
  addCellOptions = (date) => {
    const trainingDate = moment(date).format('YYYY-MM-DD');
    const trainings = this.props.trainings.filter(t => moment(trainingDate).isSame(t.date, 'day'));

    return (
      <div>
        <Link to={`/trainings/new/${trainingDate}`} className="trainings__calendar-add hover-only">
          <MdAddBox className="icon" />
        </Link>
        {
          trainings.length > 0 && (
            <Link to={`/trainings/${trainingDate}`} className="trainings__icons">
              <MdFitnessCenter className="icon" />
              <span>{ trainings.length }</span>
            </Link>
          )
        }
      </div>
    )
  }

  render() {
    return(
      <div className="dashboard">
        <div className="trainings">
          {/* <Link to="/trainings/new" className="button submit_input">
            Add Training
          </Link> */}
          <Calendar addCellOptions={this.addCellOptions} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    trainings: getTrainings(state, props),
    // search: getSearchQuery(state, props),
  }
}

export default connect(mapStateToProps)(Trainings);