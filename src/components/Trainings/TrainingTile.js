import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

export default ({ training, search }) => {
  const escapedSearch = encodeURIComponent(search);
  const link = search ? 
    `/trainings/${training.date}/${training.id}?search=${escapedSearch}` : 
    `/trainings/${training.date}/${training.id}`;
  return (
    <Link to={link} className="trainings__daily-box">
      <span>{training.date}</span>
      <FormattedMessage
        id="TrainingTile.TrainingId"
        defaultMessage="Training"
      >
        {
          placeholder => (
            <span>{ training.name || `${placeholder} ${training.id}` }</span>
          )
        }
      </FormattedMessage>
      <FormattedMessage
        id="TrainingTile.Athletes"
        defaultMessage="athletes"
      >
        {
          placeholder => (
            <small>{training.category} ({training.athletes.length} {placeholder})</small>
          )
        }
      </FormattedMessage>
    </Link>
  )
}