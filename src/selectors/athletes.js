import moment from 'moment';
import { createSelector } from 'reselect'
import { getSearchQuery } from './router';
import { getTrainings, getTrainingsThisWeek, getTrainingsThisMonth } from './training/trainingList';

export const getAthletes = (state, _) => state.athletes;

export const getAthlete = (state, props) => {
  const athleteId = (props.athlete && props.athlete.id) || (props.match.params && props.match.params.id);
  const athlete = state.athletes.find(athlete => athlete.id == athleteId);

  if(athlete) {
    const diff = moment().diff(moment(athlete.dateOfBirth), 'years');
    let category;

    // ToDo: Handle this in API
    switch (true) {
      case diff < 16:
        category = 'U16';
        break;
      case diff < 18:
        category = 'U18';
        break;
      case diff < 20:
        category = 'U20';
        break;
      case diff < 23:
        category = 'U23';
        break;
      default:
        category = 'Senior';
    }

    athlete['category'] = category;
    return athlete;
  } else {
    return { id: undefined };
  }
}

export const getAthleteTrainings = createSelector(
  [getTrainings, getAthlete], (trainings, athlete) => {
    return trainings.filter(training => (
      training.athletes.map(a => a.id).includes(athlete.id)
    ));
  }
)

export const getAthleteLastTraining = createSelector(
  [getAthleteTrainings], trainings => {
    if(trainings.length === 0) {
      return [];
    }
    return trainings.reduce((prev, current) => (
      moment(current.date).isAfter(moment(prev.date)) && current.count > 0 ? current : prev
    ))
  }
)

export const getAthleteTrainingsThisWeek = createSelector(
  [getTrainingsThisWeek, getAthlete], (trainings, athlete) => {
    return trainings.filter(training => (
      training.athletes.map(a => a.id).includes(athlete.id)
    ));
  }
)

export const getAthleteTrainingsThisMonth = createSelector(
  [getTrainingsThisMonth, getAthlete], (trainings, athlete) => {
    return trainings.filter(training => (
      training.athletes.map(a => a.id).includes(athlete.id)
    ));
  }
)

export const getFilteredAthletes = (state, props) => {
  const phrase = getSearchQuery(state, props);
  
  if(phrase) {
    return state.athletes.filter((athlete) => {  
      const fullName = `${athlete.firstName} ${athlete.lastName}`;

      const firstNameMatch = athlete.firstName.toLowerCase().includes(phrase.toLowerCase());
      const lastNameMatch = athlete.lastName.toLowerCase().includes(phrase.toLowerCase());
      const fullNameMatch = fullName.toLowerCase().includes(phrase.toLowerCase());
      const birthdayMatch = athlete.dateOfBirth.includes(phrase);

      return firstNameMatch || lastNameMatch || fullNameMatch || birthdayMatch;
    })
  } else {
    return state.athletes;
  }
}
