export const getSearchQuery = (_, props) => {
  const regex = new RegExp('search=([^&]+)');
  const search = regex.exec(props.location.search);
  return search ? search[1] : "";
}