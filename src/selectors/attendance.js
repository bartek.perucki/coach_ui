import { createSelector } from 'reselect';
import { getTrainings } from './training/trainingList';
import { getAthlete } from './athletes';
import moment from 'moment';

export const getAttendance = (state, _) => state.attendance;

export const getAthleteAttendance = (state, props) => {
  const athleteId = props.match.params.id || 0;
  return state.attendance[athleteId]
}

export const getTrainingsSinceAthleteCreated = createSelector(
  [getTrainings, getAthlete], (trainings, athlete) => {
    return trainings.filter(training => (
      moment(training.date).isSameOrAfter(moment(athlete.createdAt)) &&
      moment(training.date).isSameOrBefore(moment(new Date()))
    ));
  }
)