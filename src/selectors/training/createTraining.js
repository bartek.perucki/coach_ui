import { createSelector } from "reselect";
import { getAthletes } from "../athletes";

export const getTraining = (state, _) => state.training.create;
export const getCurrentStep = (state, _) => state.training.create.currentStep;
export const getCurrentGroup = (state, _) => {
  const { groups, currentGroup } = state.training.create;
  return currentGroup > groups.length -1 ? 0 : currentGroup;
}

export const getTrainingGroups = createSelector(
  [getTraining], training => training.groups
);

export const getTrainingGroup = createSelector(
  [getTrainingGroups, getCurrentGroup], (groups, currentGroup) => groups[currentGroup]
);

export const getTrainingGroupsWithAthletes = createSelector(
  [getTrainingGroups, getAthletes], (groups, athletes) => {
    return groups.map(group => ({
      ...group,
      athletes: group.athletes.map(id => athletes.find(athlete => athlete.id == id)),
    }));
  }
)

export const getAthletesWithNoGroup = createSelector(
  [getTrainingGroups, getAthletes], (groups, athletes) => {
    return athletes.filter(athlete => !groups.map(group => group.athletes).flat().includes(athlete.id));
  }
)
