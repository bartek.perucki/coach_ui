import { createSelector } from "reselect";
import moment from 'moment';

export const isTrainingListLoading = (state, _) => state.training.list.isLoading;

export const getTrainings = (state, _) => {
  return state.training.list.data.sort((a, b) => {
    if(moment(a.date).isAfter(moment(b.date))) {
      return -1;
    } else if(moment(a.date).isBefore(moment(b.date))) {
      return 1;
    }
    return 0;
  });
}
export const getTrainingsTotalCount = (state, _) => state.training.list.data.length;

export const getTrainingsThisWeek = createSelector(
  [getTrainings], trainings => {
    const sunday = moment().startOf('week');

    return trainings.filter(training => (
      moment(training.date).isSame(sunday, 'week')
    ));
  }
)

export const getTrainingsThisWeekCount = createSelector([getTrainingsThisWeek], trainings => trainings.length);

export const getTrainingsThisMonth = createSelector(
  [getTrainings], trainings => {
    const monthStart = moment().startOf('month');

    return trainings.filter(training => (
      moment(training.date).isSame(monthStart, 'month')
    ));
  }
)

export const getTrainingsThisMonthCount = createSelector([getTrainingsThisMonth], trainings => trainings.length);

export const getTrainingsFromDate = (state, props) => {
  const date = props.match.params.date;
  return state.training.list.data.filter(training => moment(training.date).isSame(date, 'day'));
}

export const getTraining = (state, props) => {
  const trainings = getTrainingsFromDate(state, props);
  return trainings.find(training => training.id == props.match.params.id);
}
