const INITIAL_STATE = {
  currentStep: 0,
  currentGroup: 0,
  category: "stadium",
  hour: "12:00",
  name: "",
  groups: []
};

export const INCREMENT_STEP = 'create-training/INCREMENT_STEP';
export const DECREMENT_STEP = 'create-training/DECREMENT_STEP';
export const RESET_STEP = 'create-training/RESET_STEP';
export const RESET_TRAINING = 'create-training/RESET_TRAINING';

export const SET_DATE = 'create-training/SET_DATE';
export const SET_CATEGORY = 'create-training/SET_CATEGORY';
export const SET_HOUR = 'create-training/SET_HOUR';
export const SET_NAME = 'create-training/SET_NAME';
export const ADD_TRAINING_GROUP = 'create-training/ADD_TRAINING_GROUP';
export const RESET_TRAINING_GROUPS = 'create-training/RESET_TRAINING_GROUPS';
export const SET_CURRENT_GROUP = 'create-training/SET_CURRENT_GROUP';

export const ADD_EXCERCISE = 'create-training/ADD_EXCERCISE';
export const REMOVE_EXCERCISE = 'create-training/REMOVE_EXCERCISE';
export const SET_EXCERCISE_DESCRIPTION = 'create-training/SET_EXCERCISE_DESCRIPTION';
export const SET_EXCERCISE_DESCRIPTIONS = 'create-training/SET_EXCERCISE_DESCRIPTIONS';

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INCREMENT_STEP:
      return {
        ...state,
        currentStep: state.currentStep + 1,
      };
    case DECREMENT_STEP:
      return {
        ...state,
        currentStep: state.currentStep - 1,
      };
    case RESET_STEP:
      return {
        ...state,
        currentStep: 0
      }
    case RESET_TRAINING:
      return INITIAL_STATE
    case SET_DATE:
      return {
        ...state,
        date: action.date,
      };
    case SET_CATEGORY:
      return {
        ...state,
        category: action.category,
      };
    case SET_NAME:
      return {
        ...state,
        name: action.name,
      };
    case SET_HOUR:
      return {
        ...state,
        hour: action.hour,
      };
    case ADD_TRAINING_GROUP:
      return {
        ...state,
        groups: [ ...state.groups, action.group ]
      }
    case RESET_TRAINING_GROUPS:
      return {
        ...state,
        groups: []
      }
    case SET_CURRENT_GROUP:
      return {
        ...state,
        currentGroup: action.currentGroup
      }
    case ADD_EXCERCISE:
      return {
        ...state,
        groups: state.groups.map((group, index) => {
          if(index === action.index) {
            return {
              ...group,
              excercises: [ ...group.excercises, action.excercise ]
            }
          } else {
            return group;
          }
        })
      }
    case REMOVE_EXCERCISE:
      return {
        ...state,
        groups: state.groups.map((group, index) => {
          if(index === action.index) {
            return {
              ...group,
              excercises: group.excercises.filter(excercise => excercise.id !== action.excerciseId)
            }
          } else {
            return group;
          }
        })
      }
    case SET_EXCERCISE_DESCRIPTION:
      return {
        ...state,
        groups: state.groups.map((group, index) => {
          if(index === action.index) {
            return {
              ...group,
              trainingDescription: action.description
            }
          } else {
            return group;
          }
        })
      }
    case SET_EXCERCISE_DESCRIPTIONS:
      return {
        ...state,
        groups: state.groups.map((group, index) => {
          if(index === action.index) {
            return {
              ...group,
              trainingDescription: action.descriptions[index]
            }
          }
        })
      }
    default:
      return state;
  }
};