const INITIAL_STATE = {
  data: [],
  isLoading: false
}

export const SET_TRAININGS = 'training-list/SET_TRAININGS';
export const ADD_TRAINING = 'training-list/ADD_TRAINING';
export const SET_LOADING_STATE = 'training-list/SET_LOADING_STATE';

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_TRAININGS:
      return {
        ...state,
        data: action.trainings
      }
    case ADD_TRAINING:
      return {
        ...state,
        data: [
          ...state.data,
          action.training
        ]
      }
    case SET_LOADING_STATE:
      return {
        ...state,
        isLoading: action.state
      }
    default:
      return state;
  }
}