export const SET_ATTENDANCE = 'SET_ATTENDANCE'

export default (state = [], action) => {
  switch (action.type) {
    case SET_ATTENDANCE:
      return action.attendance;
    default:
      return state;
  }
};
