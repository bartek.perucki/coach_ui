export const SET_ATHLETES = "SET_ATHLETES";
export const ADD_ATHLETE = "ADD_ATHLETE";
export const UPDATE_ATHLETE = "UPDATE_ATHLETE";
export const DELETE_ATHLETE = "DELETE_ATHLETE";

export default (state = [], action) => {
  switch (action.type) {
    case SET_ATHLETES:
      return action.athletes;
    case ADD_ATHLETE:
      return [
        ...state,
        action.athlete
      ];
    case UPDATE_ATHLETE:
      return state.map(athlete => {
        if(athlete.id === action.id) {
          return {
            ...athlete,
            ...action.updates
          }
        } else {
          return athlete;
        }
      });
    case DELETE_ATHLETE:
      return state.filter(athlete => athlete.id !== action.id);
    default:
      return state;
  }
};